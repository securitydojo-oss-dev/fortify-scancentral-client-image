# Build Fortify Scancentral Client image 

In order to build this image follow this instuctions 
Push the Fortify SCA and APPS file to your package registry.
If you add your keystore, uncomment the keystore copying in the Dockerfiles

### To push a package in your registry
using CURL
````curl -v --header "PRIVATE-TOKEN: [TOKEN]]" --upload-file ./Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run "https://gitlab.com/api/v4/projects/{$PROJECT}/{$VERSION}/{$FILENAME}?status=default" ````


## Image creation instructions
### Add/Generate missing files
If you need to use your own certificates you can add you cacerts to the resources folder.

### Build images
Type de command
````docker build -t fortify-scancentral-client:{$VERSION} -f Dockerfile .````

## Deployment 
Here is a list of environment variables and empty volumes which are needed to deploy containers
/!\ All Tokens and passwords must be deployed as Kubernetes Secrets in the same namspace as the pods you are deploying /!
Some Tokens may already be created (such as CLIENT_AUTH_TOKEN, WORKER_AUTH_TOKEN, SSC_AUTH_TOKEN)
fortify-scancentral-client:
- CLIENT_AUTH_TOKEN: Token shared between the SC Client and the Fortify SC Controller
- CA_CERTS_PASSWORD: Password of the Java KeyStore in which you must have imported the CA certificate
- FORTIFY_UPDATE_FROM_URL: the URL from which retrieve (at the starting phase) all Fortify RulePacks

## Annex

### Create a KeyStore and import the CA certificate into the KeyStore
Create a keystore with a strong password  
````keytool -genkey -v -keystore customcacerts -alias customCACerts -keyalg RSA -keysize 4096 -validity 10000````  
Then import the CA certificate (which it is named XXXX_CA_APPLICATIONS.cer)  
````keytool -importcert -file <CA_CERTIFICATE.cer> -alias <CA_NAME> -keystore customcacerts -trustcacerts````  