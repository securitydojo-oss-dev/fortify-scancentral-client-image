#!/bin/bash

# export JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts" "-Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"
# export JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Djavax.net.ssl.trustStore=$(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts" "-Djavax.net.ssl.trustStorePassword=${CA_CERTS_PASSWORD:-changeit}"

echo "client_auth_token=${CLIENT_AUTH_TOKEN:-}" > ${FORTIFY_HOME}/Core/config/client.properties 

fortifyupdate -url "${FORTIFY_UPDATE_FROM_URL:-https://update.fortify.com}" -acceptKey

exec "$@"
