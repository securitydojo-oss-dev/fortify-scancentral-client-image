FROM ubuntu:20.04

ARG FORTIFY_VERSION=22.1.1
ARG FORTIFY_HOME="/opt/fortify"
ARG CREATED_USER=ciuser
ARG RESOURCES_PATH=./resources
ARG SCALA_VERSION=2.13.8

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris


ARG PKGREGISTRY_JOB_TOKEN
ENV TOKEN=$PKGREGISTRY_JOB_TOKEN
ARG TOKEN_TYPE=JOB-TOKEN

# "${RESOURCES_PATH}/customcacerts",

# Get Packages / Binaries dependencies
RUN apt-get update && apt-get install wget -y

RUN wget --header "${TOKEN_TYPE}: ${TOKEN}" "https://gitlab.com/api/v4/projects/31978439/packages/generic/fortifysca/${FORTIFY_VERSION}/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run" -P /root/

COPY [ "${RESOURCES_PATH}/docker-entrypoint.sh", "${RESOURCES_PATH}/fortify.license", "${RESOURCES_PATH}/fortify.options", "/root/"]

RUN export DEBIAN_FRONTEND=noninteractive                                                               && \
    adduser --disabled-password --shell /bin/bash --home /home/${CREATED_USER} ${CREATED_USER}          
    # Add Entrypoints
RUN mkdir -p /entrypoints                                                                               && \
    mv /root/*-entrypoint.sh /entrypoints                                                               && \
    chmod +x /entrypoints/*                                                                             && \
    chown ${CREATED_USER}:${CREATED_USER} -R /entrypoints                                              
    # Install needed tools
RUN apt-get update                                                                                      && \
    apt-get install -y curl gnupg2 ca-certificates lsb-release apt-transport-https unzip           \
    zip git python3 python3-pip python2 git maven gradle ant software-properties-common scala
    # Add needed custom repositories
RUN wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add -            && \
    add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/                            && \
    add-apt-repository --yes ppa:ondrej/php                                                             && \
    apt-get update                                                                                      
    # Install OpenJDK 11
RUN apt-get install -y adoptopenjdk-11-hotspot                                                 
    # Install Python 2 PIP
RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py                               && \
    python2 get-pip.py                                                                                  
    # Install GCC G++ and C build tools
RUN apt-get install -y build-essential autotools-dev automake gcc g++ clang cmake                      
    # Install PHP 7.3                                         
RUN apt-get install -y php7.3 php7.3-bcmath php7.3-bz2 php7.3-cgi php7.3-cli php7.3-common              \
    php7.3-curl php7.3-dba php7.3-dev php7.3-enchant php7.3-fpm php7.3-gd php7.3-gmp                    \
    php7.3-imap php7.3-interbase php7.3-intl php7.3-json php7.3-ldap php7.3-mbstring                    \
    php7.3-mysql php7.3-odbc php7.3-opcache php7.3-pgsql php7.3-phpdbg php7.3-pspell                    \
    php7.3-readline php7.3-recode php7.3-snmp php7.3-soap php7.3-sqlite3 php7.3-sybase                  \ 
    php7.3-tidy php7.3-xml php7.3-xmlrpc php7.3-xsl php7.3-zip                                          && \ 
    update-alternatives --set php /usr/bin/php7.3                                                       
    # Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -                                             && \
    apt-get install -y nodejs                                                                           && \
    npm i -g @angular/cli @cyclonedx/bom                                                                
    # Install Composer
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php                                    && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer --version=1.10.17          


   #Install sbt
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list                       && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list                          && \
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add   && \
    apt-get update                                                                                                              && \
    apt-get install sbt

# # Certif
# RUN chown -R ${CREATED_USER} /root/customcacerts                                                        && \
# cp /root/customcacerts $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts        && \
# chmod +r $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts                      

# Install Fortify
RUN chmod u+x /root/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run
RUN /root/Fortify_SCA_and_Apps_${FORTIFY_VERSION}_linux_x64.run --mode unattended --optionfile /root/fortify.options
RUN unzip -d ${FORTIFY_HOME}/plugins/maven/maven-plugin-bin ${FORTIFY_HOME}/plugins/maven/maven-plugin-bin.zip

    # Install Lightbend license 
RUN mkdir /home/ciuser/.lightbend                                                                       && \
    cp ${FORTIFY_HOME}/plugins/lightbend/license /home/ciuser/.lightbend/license

    # Install scala fortify plugin 
WORKDIR ${FORTIFY_HOME}
RUN wget https://repo1.maven.org/maven2/com/lightbend/scala-fortify_${SCALA_VERSION}/1.0.21/scala-fortify_${SCALA_VERSION}-1.0.21.jar  && \
    cp scala-fortify_${SCALA_VERSION}-1.0.21.jar ${FORTIFY_HOME}/bin/


# Give rights and clean
RUN chown ${CREATED_USER} -R /opt/*                                                                     && \
    rm -r /root/* 

USER ${CREATED_USER}

WORKDIR ${FORTIFY_HOME}/plugins/maven/maven-plugin-bin
RUN chmod u+x ./install.sh &&                                                                            \
    ./install.sh

WORKDIR /home/${CREATED_USER}

ENV FORTIFY_HOME=${FORTIFY_HOME}                                                                          \
    FORTIFY_VERSION=${FORTIFY_VERSION}                                                                    \
    PATH=${FORTIFY_HOME}/bin:${PATH}                                                                      \
    CREATED_USER=${CREATED_USER}                                                                          \
    WORKER_AUTH_TOKEN=""

ENTRYPOINT [ "/entrypoints/docker-entrypoint.sh" ]
